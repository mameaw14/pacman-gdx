/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gdx.pacman;

/**
 *
 * @author nimo
 */
public class World {
    private Pacman pacman;
    private PacmanGame pacmanGame;
    private Maze maze;
    
    World(PacmanGame pacmanGame) {
        this.pacmanGame = pacmanGame;
	maze = new Maze();
        pacman = new Pacman(60,60,maze);
    }
 
    public Pacman getPacman() {
        return pacman;
    }
    
    public Maze getMaze(){
	return maze;
    }
    
    public void update(float delta) {
        pacman.update();
    }
}