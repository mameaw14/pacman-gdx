/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gdx.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author nimo
 */
public class WorldRenderer {
    public SpriteBatch batch;
    public static final int BLOCK_SIZE = 40;
    private PacmanGame pacmanGame;
    private Pacman pacman;
    private Texture pacmanImg;
    private World world;
    private MazeRenderer mazeRenderer;
    
    WorldRenderer(PacmanGame pacmanGame, World world) {
	this.pacmanGame = pacmanGame;
	this.world = world;
	mazeRenderer = new MazeRenderer(pacmanGame.batch,world.getMaze());
	pacman = world.getPacman();
	batch = pacmanGame.batch;
 
        pacmanImg = new Texture("pacman.png");
    }
    
    public void render(float delta){
	mazeRenderer.render();
        batch.begin();
	Vector2 pos = pacman.getPosition();
        batch.draw(pacmanImg, pos.x - BLOCK_SIZE/2, pos.y - BLOCK_SIZE/2);
        batch.end();
    }
}
